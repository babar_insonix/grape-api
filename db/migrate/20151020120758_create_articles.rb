class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.string :author_name
      t.string :comment
      t.timestamps
    end
  end
end
