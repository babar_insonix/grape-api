module API
  module V1
    class Users < Grape::API
      include API::V1::Defaults
      resource :users do

        desc 'Show all users'
        get :show_all do
          present Api::User.all, with: Entities::UserEntity
        end

        desc 'User sign in'
        params do
          requires :email, :password, type: String
        end
        get :sign_in do
          user = Api::User.find_by_email(params[:email])
          if user && user.valid_password?(params[:password])
            present user, with: Entities::UserEntity
          else
            error!({:error_code => 404, :error_message => "Invalid email or password."}, 401)
          end
        end

        desc 'User sign up'
        params do
          optional :first_name, type: String, desc: "First Name"
          optional :last_name, type: String, desc: "Last Name"
          optional :department, type: String, desc: "Department"
          requires :email, type: String, desc: "Email"
          requires :password, type: String, desc: "Password"

        end

        post :sign_up do
          user = Api::User.new(
              first_name: params[:first_name],
              last_name: params[:last_name],
              department: params[:department],
              password: params[:password],
              email: params[:email]
          )

          if user.valid?
            user.save
            present user, with: Entities::UserEntity
          else
            error!({:error_code => 404, :error_message => "#{user.errors.full_messages.to_sentence}"}, 401)
          end
        end

        desc 'Update user profile'
        params do
          requires :id, type: Integer, desc: 'User Id'
          requires :first_name, type: String, desc: 'First Name'
          requires :last_name, type: String, desc: 'Last Name'
          requires :department, type: String, desc: 'Department'
        end

        put :update_profile do
          user = Api::User.find_by_id(params[:id])
          if user.present?
            user.assign_attributes(:first_name => params[:first_name], :last_name => params[:last_name], :department => params[:department])
            user.save
            present user, with: Entities::UserEntity
          else
            error!({:error_code => 404, :error_message => 'Invalid user'}, 401)
          end
        end

        desc 'Delete user'
        params do
          requires :id, type: Integer, desc: 'User Id'
        end
        delete :delete_user do
          user = Api::User.find_by_id(params[:id])
          if user.present?
            user.destroy
            error!({:success_code => 200, :success_message => 'User deleted successfully'}, 201)
          else
            error!({:error_code => 404, :error_message => 'Record not found'}, 401)
          end
        end

      end

    end
  end
end