module API
  module V1
    class Articles < Grape::API
      include API::V1::Defaults

      resource :articles do
        desc 'Get list of articles'
        get :show_all do
          present Article.all,with: Entities::ArticleEntity
        end

        desc 'Create new article'
        params do
          requires :name, type: String, desc: 'Name'
          requires :author_name, type: String, desc: 'Author_name'
          requires :comment, type: String, desc: 'Comment'
        end
        post :create do
         present Article.create(:name => params[:name], :author_name => params[:author_name], :comment => params[:comment]),with: Entities::ArticleEntity
        end
      end
    end
  end
end