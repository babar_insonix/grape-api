module API
  module V1
    class Auth < Grape::API
      resource :auth do
        desc 'Auth sign in to get API key'
        params do
          requires :email,:password, type:String
        end
        post :sign_in do
          user = User.find_by_email(params[:email])
          return present user,with: Entities::AuthEntity if user && user.valid_password?(params[:password])
          error!("401 Unauthorized", 401)
        end
      end

    end
  end
end