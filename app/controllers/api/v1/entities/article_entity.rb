module API
  module V1
    module Entities
      class ArticleEntity < Grape::Entity
        expose :name
        expose :author_name
        expose :comment
      end
    end
  end
end