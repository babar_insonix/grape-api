module API
  module V1
    module Entities
      class AuthEntity < Grape::Entity
        expose :api_key
      end
    end
  end
end