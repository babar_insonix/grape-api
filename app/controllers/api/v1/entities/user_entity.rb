module API
  module V1
    module Entities
      class UserEntity<Grape::Entity
        expose :first_name
        expose :last_name
        expose :email
        expose :department
      end
    end
  end
end